
/**
 * @file Provides for the necessary Javascript for the taxonomy_widget module.
 */

/**
 * Handle the vocabulary change event for the taxonomy select widget. The
 * handler queries the taxonomy widget callback with the selected vocabulary
 * ID and populates the term select element with the results.
 *
 * @param object event The select element's change event.
 */
function taxonomyWidgetVocabularyChange(event) {
  var $target     = $(event.target);
  var vid         = $target.val();
  var $termSelect = $target.parent().siblings('div.form-item').children('select.taxonomy-widget-term');
  var term        = $termSelect.val();

  if (vid > 0) {
    // Populate the term select with this vocabulary's terms.
    $.getJSON(event.data.callback + "/" + vid, function(data) {
      var count = 0;

      $termSelect.empty();

      for (var tid in data) {
        found = false;

        // If multiple items are selected, the term value is an array.
        if (term instanceof Array) {
          for (i = 0; i < term.length; i++) {
            if (tid == term[i]) {
              found = true;
              break;
            }
          }
        }
        // Otherwise we can just check for equality.
        else {
          found = (tid == term);
        }

        // Generate the options for the select element.
        if (found) {
          $termSelect.append('<option value="' + tid + '" selected>' + data[tid] + "</option>");
        }
        else {
          $termSelect.append('<option value="' + tid + '">' + data[tid] + "</option>");
        }

        count++;
      }

      // Resize after populating using the same algorithm as Drupal.
      if ($termSelect.attr("multiple") == "multiple") {
        $termSelect.attr("size", Math.min(9, count));
      }
    });
  }
  else {
    $termSelect.html('<option value="0">Select a vocabulary.</option>');
    
    if ($termSelect.attr("multiple") == "multiple") {
      $termSelect.attr("size", 3);
    }
  }
}
